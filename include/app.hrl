%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021

%% ====================================================================
%% Types
%% ====================================================================

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    %
    subscrid :: binary(),
    %
    ref :: reference(),
    timerref :: reference(),
    %
    ets :: ets:tab(),
    %
    is_paused = false :: boolean(),
    pause_reason :: term(),
    %
    stored_events = [] :: list(),
    %
    search_pid :: pid(),
    search_monref :: reference(),
    modify_pid :: pid(),
    modify_monref :: reference(),
    %
    start_ts :: non_neg_integer(),
    loaded_ts :: non_neg_integer()
}).

%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(ClassesCN, <<"classes">>).
-define(SvcScriptsCN, <<"svcscripts">>).

-define(MasterStorageInstance, <<"master">>).
-define(MasterStorageType, <<"postgresql">>).
-define(MasterStorageMode, <<"category">>).

-define(ScriptType, <<"svc">>).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(SCRIPTLIB, scriptlib).
-define(PLATFORMLIB, platformlib).

-define(MsvcDms, <<"dms">>).
-define(MsvcStore, <<"store">>).

-define(APP, svcsc).
-define(MsvcType, <<"svcsc">>).

-define(SUPV, svcsc_supv).
-define(CFG, svcsc_config).
-define(DomainTreeCallback, svcsc_domaintree_callback).
-define(ScriptlibCallback, svcsc_scriptlib_callback).

-define(DSUPV, svcsc_domain_supv).
-define(DSRV, svcsc_domain_srv).

-define(VALIDATOR, svcsc_validator_srv).
-define(ValidatorSvcScripts, svcsc_validator_coll_svcscripts).

-define(FixturerCallback, svcsc_fixturer_callback).
-define(FixturerLeaderName, 'svcsc_fixturer_leader_srv').
-define(FixturerClasses, svcsc_fixture_coll_classes).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).

%% ------
%% From platformlib
%% ------
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(LeaderSupv, platformlib_leader_app_supv).
-define(LeaderU, platformlib_leader_app_utils).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CRUD, platformlib_dms_crud).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_FIXTURER_SUPV, platformlib_dms_fixturer_leader_supv).

-define(DTREE_SUPV, platformlib_domaintree_supv).

%% ------
%% From scriptlib
%% ------
-define(SMSRV, scriptlib_sm_srv).


%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {script,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================
