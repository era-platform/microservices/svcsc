%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Controlled classes descriptions (for dms collection 'classes')

-module(svcsc_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([svcscripts/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "svcscripts"
%% return fixtured entity of controlled class
%% -------------------------------------
svcscripts(_Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"6324f223-017b-3461-1b67-7cd30a921f58">>),
        <<"classname">> => ?SvcScriptsCN,
        <<"name">> => <<"Service scripts">>,
        <<"description">> => <<"General platform collection. Service scripts.">>,
        <<"storage_mode">> => <<"category">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => ?BU:strbin("~ts:validator",[?MsvcType]),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"lookup_properties">> => [<<"code">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"code">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"name">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"scriptdata">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"lwt">>,
                <<"data_type">> => <<"datetime">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"opts">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================