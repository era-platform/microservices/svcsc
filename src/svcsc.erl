%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Service script/scenario microservice app.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'script','svcsc'}

-module(svcsc).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0]).

-export([start/2,
         stop/1]).

-export([start_script/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Start method
%% -------------------------------------------
-spec start() -> ok | {error,Reason::term()} | {retry_after,Timeout::non_neg_integer(),Reason::term()}.
%% -------------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% ====================================================================
%% API functions
%% ====================================================================

start_script(_Domain,_Args) -> false.

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    ?OUT('$info',"~ts. ~p start", [?APP, self()]),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts: Application stopped (~120p)", [?APP, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% @private
setup_dependencies() ->
    % scriptlib
    ?BU:set_env(?SCRIPTLIB, 'log_destination', {script,scriptlib}),
    ?BU:set_env(?SCRIPTLIB, 'temp_path', ?BU:str("/tmp/scripts/~ts",[node()])),
    ?BU:set_env(?SCRIPTLIB, 'site', ?PCFG:get_current_site()),
    ?BU:set_env(?SCRIPTLIB, 'script_type', ?ScriptType),
    ?BU:set_env(?SCRIPTLIB, 'meta_module', ?ScriptlibCallback),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that out of app link
%% --------------------------------------
ensure_deps_started() ->
    application:ensure_all_started(?PLATFORMLIB, permanent),
    application:ensure_all_started(?SCRIPTLIB, permanent).
