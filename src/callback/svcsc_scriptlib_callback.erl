%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Callback module for scriptlib

-module(svcsc_scriptlib_callback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    components/0,
    expression_functions/0,
    expression_function/2,
    icon/1,
    get_dynamic_list/3,
    get_script/2,
    start_async_script/3,
    storage_put/3,
    storage_get/2,
    storage_delete/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------
%% Metadata functions
%% -----------------------------------

%% TODO: implement
components() -> #{}.

%% TODO: implement
expression_functions() -> [].

%% TODO: implement
icon(Type) -> undefined.

%% TODO: implement
get_dynamic_list(Domain,ScriptType,Key) -> [].

%% -----------------------------------
%% Execute external function on arguments
%% -----------------------------------

%% TODO: implement
expression_function(FunName,Arity) -> undefined.

%% -----------------------------------
%% Search script of same type and new code
%% -----------------------------------

get_script(Domain,Code) ->
    case catch ?GLOBAL:gen_server_call(Domain,{get_script,Code}) of
        {'EXIT',Reason} -> {error,Reason};
        false -> {error,<<"Script not found">>};
        {ok,_SvcScript}=Ok -> Ok
    end.

%% -----------------------------------
%% Start script in async mode (service script)
%% -----------------------------------

start_async_script(Domain,Code,Variables) ->
    case catch ?GLOBAL:gen_server_call(Domain,{get_script,Code}) of
        {'EXIT',Reason} -> {error,Reason};
        false -> {error,<<"Script not found">>};
        {ok,SvcScript} ->
            Res = ?SMSRV:start(#{type => ?ScriptType,
                                 domain=> Domain,
                                 code => Code,
                                 script => SvcScript,
                                 loglevel => 6,
                                 initial_var_values => Variables}),
            case Res of
                {ok,Id,Pid} -> {ok,#{id => Id,
                                     pid => Pid}};
                {error,_}=Err -> Err
            end end.

%% -----------------------------------
%% Storage operations
%% -----------------------------------

%%
storage_put(Key,Value,TTLms) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcStore),
    case catch ?GLOBAL:gen_server_call(GlobalName,{put,[Key,Value,TTLms]}) of
        {'EXIT',Reason} ->
            ?LOG('$error',"expression function 'storage_put/3 crashed: ~120tp",[Reason]),
            undefined;
        {error,Reason} ->
            ?LOG('$error',"expression function 'storage_put/3 error: ~120tp",[Reason]),
            undefined;
        ok -> ok
    end.

%%
storage_get(Key,Default) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcStore),
    case catch ?GLOBAL:gen_server_call(GlobalName,{get,[Key]}) of
        {'EXIT',Reason} ->
            ?LOG('$error',"expression function 'storage_get/2 crashed: ~120tp",[Reason]),
            Default;
        {error,Reason} ->
            ?LOG('$error',"expression function 'storage_get/2 error: ~120tp",[Reason]),
            Default;
        {Key,Value} -> Value
    end.

%%
storage_delete(Key) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcStore),
    case catch ?GLOBAL:gen_server_call(GlobalName,{delete,[Key]}) of
        {'EXIT',Reason} ->
            ?LOG('$error',"expression function 'storage_del/1 crashed: ~120tp",[Reason]),
            undefined;
        {error,Reason} ->
            ?LOG('$error',"expression function 'storage_del/1 error: ~120tp",[Reason]),
            undefined;
        ok -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================