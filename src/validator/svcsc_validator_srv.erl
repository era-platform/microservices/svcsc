%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Server for validation of entities in controlled collections

-module(svcsc_validator_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    ?GLOBAL:gen_server_start_link({gnr,multi,?BU:strbin("~ts:validator",[?MsvcType])}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    State1 = undefined,
    ?LOG('$info', "~ts. Validator srv inited", [?APP]),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% validation request
handle_info({'$call', {ReplyTo,Ref}, {validate, {Domain, CN, Operation, EntityInfo, ModifierInfo}}}, State) ->
    Reply = try case CN of
                    ?SvcScriptsCN -> ?ValidatorSvcScripts:validate(Domain,Operation,EntityInfo,ModifierInfo);
                    _ -> {error,{internal_error,<<"Invalid validator service">>}}
                end
            catch
                throw:R -> R;
                E:R:ST ->
                    ?LOG('$error',"Validation crash: {~120tp,~120tp}~n\tStack: ~160tp",[E,R,ST]),
                    {error,{500,<<"Validation crashed">>}}
            end,
    ReplyTo ! {'$reply',Ref,Reply},
    {noreply,State};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================