%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.05.2021
%%% @doc Validates 'svcscripts' entity on any modify operation.

-module(svcsc_validator_coll_svcscripts).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=_EntityInfo,_ModifierInfo) ->
    FunIsFixt = fun(_E) -> false end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        _ when Operation=='delete' ->
            {ok,undefined};
        _ ->
            case check_entity(Domain,Operation,E1) of
                {error,_}=Err -> Err;
                {ok,EntityV} ->
                    case validate_all_items(Domain,Operation,{E0,EntityV}) of
                        ok -> {ok,EntityV};
                        {error,_}=Err -> Err
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check class properties
%% -------------------------------------
check_entity(Domain,Operation,Item) ->
    check_1(Domain,Operation,Item).

%% @private
%% code
check_1(Domain,Operation,Item) ->
    case maps:get(<<"code">>,Item) of
        <<>> -> {error, {invalid_params, <<"field=code|Invalid 'code'. Must not be empty">>}};
        Code when is_binary(Code) ->
            F = fun(A) when (A==$/) orelse (A==$!)
                        orelse (A>=$a andalso A=<$z)
                        orelse (A>=$A andalso A=<$Z)
                        orelse (A>=$0 andalso A=<$9)
                        orelse (A==$_) orelse (A==$-) orelse (A==$~) -> true;
                   (_) -> false end,
            case lists:all(F, ?BU:to_list(Code)) of
                true -> check_1b(Domain,Operation,Item);
                false -> {error, {invalid_params, <<"field=code|Invalid code. Contains invalid symbols. Expected: [A-Za-z0-9_-~/!]">>}}
            end end.
%% name
check_1b(Domain,Operation,Item) ->
    case maps:get(<<"code">>,Item) of
        <<>> -> {error, {invalid_params, <<"field=code|Invalid 'code'. Must not be empty">>}};
        Code when is_binary(Code) -> check_2(Domain,Operation,Item)
    end.

%% scriptdata
check_2(Domain,Operation,Item) ->
    ScriptData = maps:get(<<"scriptdata">>,Item),
    case maps:get(<<"objects">>,ScriptData,undefined) of
        [_|_] -> check_3(Domain,Operation,Item);
        _ -> {error, {invalid_params, <<"field=scriptdata|Invalid scriptdata. Expected map containing 'objects' as non-empty list">>}}
    end.
%% opts
check_3(Domain,Operation,Item) ->
    Opts = maps:get(<<"opts">>,Item),
    DefaultOpts = default_opts(),
    case check_opts(Domain,Operation,maps:merge(DefaultOpts,maps:with(maps:keys(DefaultOpts), Opts)),Item) of
        {error,_}=Err -> Err;
        Opts1 -> check_x(Domain,Operation,Item#{<<"opts">> => Opts1})
    end.
%
check_x(_Domain,_Operation,Item) -> {ok,Item}.

%% -------------------------------------
%% Check opts
%% -------------------------------------

%% @private
default_opts() ->
    #{
        <<"comment">> => <<>>,
        <<"title">> => <<>>,
        % common
        <<"loglevel">> => 0, % 0-6
        <<"variables">> => #{}
    }.

%% @private
check_opts(_Domain,Operation,Opts,Item) ->
    try
        maps:fold(fun(_K,_V,Acc) when Operation=='fixture' -> Acc;
                     (<<"comment">>,_V,Acc) -> Acc;
                     (<<"title">>,_V,Acc) -> Acc;
                     (<<"loglevel">>=K,V,Acc) ->
                         case ?BU:to_int(V,undefined) of
                             I when is_integer(I), I >= 0, I =< 6 -> Acc#{K => I};
                             _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected integer 0-6",[K,K])}})
                         end;
                     (<<"variables">>=K,V,Acc) ->
                         case is_map(V) of
                             true -> Acc;
                             false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected object (VarName => Value)",[K,K])}})
                         end;
                     (_K,_V,Acc) -> Acc
                  end, Opts, Opts)
    catch
        error:R:ST ->
            ?LOG('$crash',"Validator crashed on check_opts: ~120tp~n\tItem: ~160tp~n\tStack: ~160tp",[{error,R},Item,ST]),
            {error, {invalid_params, <<"field=opts|Invalid 'opts'. Could not parse">>}};
        throw:{error,_}=Err -> Err
    end.

%% -------------------------------------
%% Check all classes in complex
%% -------------------------------------
validate_all_items(Domain,Operation,{_E0,Item}) ->
    Id = maps:get(<<"id">>,Item),
    CN = maps:get(<<"code">>,Item),
    AllItems = case ?DMS_CACHE:read_cache(Domain,?SvcScriptsCN,#{},auto) of
                     {ok,Items,_} -> lists:filter(fun(ItemF) -> maps:get(<<"id">>,ItemF) /= Id end, Items);
                     T ->
                         ?LOG('$error',"Validator read svcscripts: ~120tp",[T]),
                         {error,{internal_error,<<"Validator cannot read svcscripts">>}}
                 end,
    case Operation of
        'delete' -> ok;
        O when O=='create';O=='replace';O=='update';O=='delete' ->
            case lists:filter(fun(ItemF) -> maps:get(<<"code">>,ItemF) == CN end, AllItems) of
                [_|_] -> {error, {invalid_params, <<"field=code|code already exists">>}};
                [] -> ok
            end end.
