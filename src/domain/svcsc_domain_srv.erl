%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc General facade of domain's svc script service.
%%%        Has global name, handles all outside requests.
%%%        Starts svc script machine
%%%        Subscribes on changes of DC's classes collection.
%%%      Opts:
%%%        regname, domain

-module(svcsc_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(TimeoutInit, 10000).
-define(TimeoutRegular, 60000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Name, Opts) when is_map(Opts) ->
    ?GLOBAL:gen_server_start_link({global, Name}, ?MODULE, Opts#{'name' => Name}, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    SyncRef = ?BU:get_by_key(sync_ref,Opts),
    Self = self(),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    subscrid = SubscrId=?BU:strbin("sid_SVCSC:~ts;~ts",[node(),Domain]),
                    ref = Ref,
                    start_ts = ?BU:timestamp(),
                    ets = ?BU:get_by_key(ets,Opts)},
    % ----
    ?BLstore:store_u({?APP,sync,Domain},{'facade',SyncRef,self()}),
    Self ! {'init',Ref},
    % ----
    SubscrOpts = #{<<"ttl_first">> => 20,
                   fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?SvcScriptsCN,self(),SubscrId,SubscrOpts),
    % ----
    ?LOG('$info', "~ts. '~ts' srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% Debug restart
handle_call({restart}, _From, State) ->
    {stop,State};

%% --------------
%% Other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% -------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init
handle_info({init,Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',SyncRef,Report}, #dstate{sync_ref=SyncRef,timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    State1 = case Report of
                 ok ->
                     Timeout = case ?BU:timestamp() of
                                   NowTS when LoadedTS/=undefined, NowTS - LoadedTS < 5000 -> LoadedTS + 5000 - NowTS;
                                   _ -> 0
                               end,
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#dstate{ref=Ref1,
                                  timerref = erlang:send_after(Timeout, self(), {timer,init,Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'init',Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'regular',Ref}, #dstate{ref=Ref}=State) ->
    Ref1 = make_ref(),
    State1 =  State#dstate{ref=Ref1,
                           timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
    {noreply, State1};

%% --------------
%% On notify from DMS about changes in subscriptions
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId,is_paused=true,stored_events=StoredEvents}=State) ->
    State1 = State#dstate{stored_events=[EventInfo|StoredEvents]},
    {noreply, State1};
%%
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId}=State) ->
    State1 = apply_changed_event(EventInfo, State),
    {noreply, State1};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------
%% initialization of data. Load all svcscripts from DMS.
%% ------------------------------------------
init_data(#dstate{}=State) ->
    State1 = State#dstate{stored_events=[]},
    Ref1 = make_ref(),
    case catch load(State1) of
        {ok,State2} ->
            State3 = State2#dstate{is_paused = false,
                                   pause_reason = undefined,
                                   loaded_ts = ?BU:timestamp(),
                                   ref=Ref1,
                                   timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
                                   apply_changed_events(State3);
        {error,_}=Err ->
            State1#dstate{is_paused = true,
                          pause_reason = Err,
                          ref=Ref1,
                          timerref = erlang:send_after(?TimeoutInit, self(), {timer,init,Ref1})}
    end.

%% @private
load(#dstate{domain=Domain,ets=Ets}=State) ->
    ets:delete_all_objects(Ets),
    ok = ?DMS_CACHE:read_all_by_portions(Domain,?SvcScriptsCN,{<<"timestamp">>,<<"desc">>},[],10000,Ets),
    ets:foldl(fun({Id,Item},_) ->
                    Code = maps:get(<<"code">>,Item),
                    ets:insert(Ets,{{id,Id},shrink(Item)}),
                    ets:insert(Ets,{{code,Code},Id})
              end, ok, Ets),
    {ok,State}.

%% @private
shrink(Item) -> maps:without([],Item).

%% ------------------------------------------
%% Applying of events about changes in subscriptions
%% ------------------------------------------
apply_changed_events(#dstate{stored_events=StoredEvents}=State) ->
    State1 = lists:foldr(fun(EventInfo,StateX) -> apply_changed_event(EventInfo,StateX) end, State, StoredEvents),
    State1#dstate{stored_events=[]}.

%% Apply one event
apply_changed_event(EventInfo, #dstate{domain=Domain,ets=Ets,timerref=TimerRef}=State) ->
    Data = maps:get(<<"data">>,EventInfo),
    case maps:get(<<"operation">>,Data) of
        'clear' ->
            ets:delete_all_objects(Ets),
            State;
        'reload' ->
            ?BU:cancel_timer(TimerRef),
            Ref1 = make_ref(),
            State#dstate{ref=Ref1,
                         timerref = erlang:send_after(?BU:random(5000), self(), {timer,init,Ref1})};
        O ->
            Item = maps:get(<<"entity">>,Data),
            Id = maps:get(<<"id">>,Item),
            Code = maps:get(<<"code">>,Item),
            case O of
                'create' ->
                    ets:insert(Ets,{{id,Id},shrink(Item)}),
                    ets:insert(Ets,{{code,Code},Id}),
                    State;
                'update' ->
                    case ets:lookup(Ets,{id,Id}) of
                        [] -> ok;
                        [{_,Item0}] ->
                            case maps:get(<<"code">>,Item0) of
                                Code -> ok;
                                Code0 -> ets:delete(Ets,{code,Code0})
                            end end,
                    ets:insert(Ets,{{id,Id},shrink(Item)}),
                    ets:insert(Ets,{{code,Code},Id}),
                    State;
                'delete' ->
                    ets:delete(Ets,{id,Id}),
                    ets:delete(Ets,{code,Code}),
                    State;
                'corrupt' ->
                    spawn(fun() -> on_corrupt(Domain,Item,Ets) end),
                    State
            end
    end.

%% @private
%% Async process
on_corrupt(Domain,Item,Ets) ->
    Id = maps:get(<<"id">>,Item),
    Code = maps:get(<<"code">>,Item),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Args = {'read',#{class => ?SvcScriptsCN,
                     object => {'entity',Id,[]},
                     qs => #{}}},
    case catch ?GLOBAL:gen_server_call(GlobalName,{crud,?SvcScriptsCN,Args}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Call to DMS (read svcscript '~ts', id='~ts', code='~ts') crashed: ~n\t~120tp",[Domain,Id,Code,Reason]),
            ok;
        {error,_}=Err ->
            ?LOG('$error',"Call to DMS (read svcscript '~ts', id='~ts', code='~ts') error: ~n\t~120tp",[Domain,Id,Code,Err]),
            case ets:lookup(Ets,{id,Id}) of
                [] -> ok;
                [{_,Item0}] ->
                    Code0 = maps:get(<<"code">>,Item0),
                    ets:delete(Ets,{id,Id}),
                    ets:delete(Ets,{code,Code0})
            end;
        {ok,Item1} ->
            case ets:lookup(Ets,{id,Id}) of
                [] -> ok;
                [{_,Item0}] ->
                    Code0 = maps:get(<<"code">>,Item0),
                    ets:delete(Ets,{code,Code0})
            end,
            Code1 = maps:get(<<"code">>,Item1),
            ets:insert(Ets,{{id,Id},shrink(Item1)}),
            ets:delete(Ets,{{code,Code1},Id})
    end.
