%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Supervisor of domain's svc script service
%%%      Supervises group of relevant processes concerned to domain
%%%      Opts:
%%%        domain, is_local

-module(svcsc_domain_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1, get_linkname/1, get_regname/1]).
-export([start_child/2, restart_child/2, terminate_child/2, delete_child/2, get_childspec/2]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions (start)
%% ====================================================================

%% starts supv
start_link(Opts) when is_map(Opts) ->
    Domain = ?BU:get_by_key('domain', Opts),
    RegName = get_linkname(Domain),
    supervisor:start_link({local,RegName}, ?MODULE, Opts).

%% returns name of supv for linking to top supv
get_linkname(Domain) when is_binary(Domain); is_atom(Domain) ->
    get_regname(Domain).

%% returns reg name of supv
get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("dms:supv;dom:~ts",[Domain])).

%% ====================================================================
%% API functions (ext management)
%% ====================================================================

start_child(Domain, ChildSpec) ->
    SupvRegName = get_regname(Domain),
    supervisor:start_child(SupvRegName, ChildSpec).

restart_child(Domain, Id) ->
    SupvRegName = get_regname(Domain),
    supervisor:restart_child(SupvRegName, Id).

terminate_child(Domain, Id) ->
    SupvRegName = get_regname(Domain),
    supervisor:terminate_child(SupvRegName, Id).

delete_child(Domain, Id) ->
    SupvRegName = get_regname(Domain),
    supervisor:delete_child(SupvRegName, Id).

get_childspec(Domain, Id) ->
    SupvRegName = get_regname(Domain),
    supervisor:get_childspec(SupvRegName, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) when is_map(Opts) ->
    Domain = ?BU:get_by_key('domain', Opts),
    FacadeSrvName = ?GN_NAMING:get_globalname(?MsvcType, Domain),
    Opts2 = Opts#{'regname' => FacadeSrvName,
                  'ets' => ets:new(ets,[public,set])},
    FacadeSrv = {?DSRV, {?DSRV, start_link, [FacadeSrvName, Opts2]}, permanent, 1000, worker, [?DSRV]},
    % -----------
    ?LOG('$info', "~ts. '~ts' supv inited", [?APP, Domain]),
    {ok, {{one_for_all, 10, 2}, [FacadeSrv]}}.