%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc

-module(svcsc_domaintree_callback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([app/1,
         log/3,
         domain_child_spec/2,
         filter_domain/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

app(_) -> subscr.

log(_,Fmt,Args) -> ?LOG('$info',Fmt,Args).

domain_child_spec(_,Domain) ->
    #{start => {?DSUPV,start_link,[#{'domain' => Domain}]},
      restart => permanent,
      shutdown => 1000,
      type => supervisor,
      modules => [?DSUPV]}.

filter_domain(_,_Domain,_DomainItem) -> true.

%% ====================================================================
%% Internal functions
%% ====================================================================